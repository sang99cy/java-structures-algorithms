package com.quangsang.datastructuresalgorithm;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DataStructuresAlgorithmApplication {

    public static void main(String[] args) {
        SpringApplication.run(DataStructuresAlgorithmApplication.class, args);
    }

}
